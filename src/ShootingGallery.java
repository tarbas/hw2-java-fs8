import java.util.Random;
import java.util.Scanner;

public class ShootingGallery {

    private static int dimX = 6;
    private static int dimY = 6;
    private static int aimX;
    private static int aimY;
    private static int shotX;
    private static int shotY;

    public static void printTarget(String[][] target ) {
        for (int i = 0; i < dimX; i++) {
            for (int j = 0; j < dimY; j++) {
                System.out.print(target[i][j] + "\t");
            }
            System.out.println();
        }
    }

    public static boolean isOutOftarget(int shot, int dim) {
        if (shot > 0 && shot < dim) return true;
        else return false;
    }

        public static void main(String[] args) {
            Scanner in = new Scanner(System.in);
            Random random = new Random();

                aimX = random.nextInt(dimX-1);
                aimY = random.nextInt(dimY-1);
                aimX +=1;
                aimY +=1;

             String[][] target  = new String[dimX][dimY];

            for (int i = 0; i < dimX; i++) {
                for (int j = 0; j < dimY; j++) {
                    int sum = i+j;
                    String sumInt = Integer.toString(sum);
                    if(i==0 || j==0) target[i][j] = sumInt;
                    else target[i][j] = "-";
                }
            }
            printTarget(target);

            do {
                do {
                    //Проверку на не число не делал - не было в задании

                    System.out.println("Enter X shot");
                    shotX = in.nextInt();
                    System.out.println("Enter Y shot");
                    shotY = in.nextInt();
                }
                while (!(isOutOftarget(shotX, dimX) && isOutOftarget(shotY, dimY)));

                if  (shotX == aimX && shotY == aimY) {
                    System.out.println("BOOOOOM!");
                    target[aimY][aimX] = "X";
                    printTarget(target);
                    break;
                }
                else target[shotY][shotX] = "*";
                printTarget(target);
            }
            while (true);
        }
}
